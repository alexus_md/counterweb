package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class CounterTest {

	@Test
	public void testDevideAbyB() throws Exception {

		assertEquals( 2, new Counter().devide( 4, 2));
	}

	@Test
	public void testDevideAbyZero() throws Exception {
		assertEquals( Integer.MAX_VALUE, new Counter().devide( 4, 0));
	}
}
