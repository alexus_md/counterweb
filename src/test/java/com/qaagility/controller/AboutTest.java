package com.qaagility.controller;

import org.junit.Test;
import org.springframework.ui.ModelMap;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class AboutTest {

	@Test
	public void testDesc() throws Exception {

		String result = new About().desc();
		assertTrue(result.contains("This application was copied from somewhere,"));
	}
}
